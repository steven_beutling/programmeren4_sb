<?php
        $voornaamErr=$naamErr=$aantalPersonenErr=$geboortedatumErr=$rekeningnummerErr=$emailErr="";
        $aantalPersonenErr="";
        $voornaam=$naam=$aantalPersonen=$geboortedatum=$rekeningnummer=$email="";
        $aantalPersonen="";
        
        if($_SERVER["REQUEST_METHOD"] == "POST")
        {
             if(empty($_POST["voornaam"])){$voornaamErr="Voornaam moet ingevult zijn!";}
             else
             {
                 if(preg_match("/^[a-zA-Z]+$/", $_POST["voornaam"])){$voornaam=$_POST["voornaam"];}
                 else{$voornaamErr="Voornaam mag alleen letters bevatten!";}
                 
             }
             
             if(empty($_POST["naam"])){$naamErr="Naam moet ingevult zijn!";}
             else
             {
                 if(preg_match("/^[a-zA-Z]+$/", $_POST["naam"])){$voornaam=$_POST["naam"];}
                 else{$voornaamErr="Naam mag alleen letters bevatten!";}
                 
             }
             
             if(empty($_POST["aantalPersonen"])){$aantalPersonenErr="Aantal personen moet ingevult zijn!";}
             else
             {
                 if(preg_match("/^[1-9]\d*$/",$_POST["aantalPersonen"]) && $_POST["aantalPersonen"] != 0 ){$aantalPersonen = $_POST["aantalPersonen"];}
                 else{$aantalPersonenErr="De waarde die u hebt ingevuld is foutief!";}
             }
             
             if(empty($_POST["geboortedatum"])){$geboortedatumErr="Geboortedatum moet ingevult zijn!";}
             else{$geboortedatum=$_POST["geboortedatum"];}
             
             if(empty($_POST["rekeningnummer"])){$rekeningnummerErr="Rekeningnummer moet ingevult zijn!";}
             else
             {
                 if(preg_match("/[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}/", $_POST["rekeningnummer"]))
                 {$rekeningnummer = $_POST["rekeningnummer"];}
                 else{$rekeningnummerErr="Rekeningnummer voldoet niet aan de eisen!";}
             }
             
             if(empty($_POST["email"])){$emailErr="Email moet ingevult zijn!";}
             else{$email=$_POST["email"];}
             
             
             
             
             
        }
        
    ?>


<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Inschrijvingsformulier</title>
</head>
<body>
    
    
    <h1>Bob Dylan concert</h1>
    <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        
        <label for="voornaam">Voornaam</label>
        <input type="text" name="voornaam" pattern="^[a-zA-Z]+$"/>
        <span class="error"><?php echo $voornaamErr; ?></span>
        <br/><br/>
        
        <label for="naam">Naam</label>
        <input type="text" name="naam" pattern="^[a-zA-Z]+$"/>
        <span class="error"><?php echo $naamErr; ?></span>
        <br/><br/>
        
        <label for="plaats">Plaats</label> <br/>
        <input type="radio" name="plaats" value="stage"/> Stage <br/>
        <input type="radio" name="plaats" value="tribune"/> Tribune <br/>
        <input type="radio" name="plaats" value="balkon"/> Balkon 
        <br/><br/>
        
        <label for="aantalPersonen">Aantal personen</label>
        <input type="number" name="aantalPersonen" pattern="^[1-9]\d*$"/>
        <span class="error"><?php echo $aantalPersonenErr; ?></span>
        <br/><br/>
        
        <label for="geboortedatum">Geboortedatum</label>
        <input type="date" name="geboortedatum"/>
        <span class="error"><?php echo $geboortedatumErr; ?></span>
        <br/><br/>
        
        <label for="rekeningnummer">Rekeningnummer</label>
        <input type="text" name="rekeningnummer" pattern="[a-zA-Z]{2}[0-9]{2}[a-zA-Z0-9]{4}[0-9]{7}([a-zA-Z0-9]?){0,16}" placeholder="BExxxxxxxxxxxxxx"/>
        <span class="error"><?php echo $rekeningnummerErr; ?></span>
        <br/><br/>
        
        <label for="email">Email</label>
        <input type="email" name="email" placeholder="Name.Lastname@gmail.com"/>
        <span class="error"><?php echo $emailErr; ?></span>
        <br/><br/>
        
        <button type="submit">Verzenden</button>
        
        
        
        
        
        
        
        
    </form>
    
    
    
</body>
</html>