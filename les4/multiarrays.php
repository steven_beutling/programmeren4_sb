<?php


    $persons= array(
        array('Johanna', 'Den Doper', 117, 'Vrouw'),
        array('Mohamed', 'El Farisi', 40, 'Man')
        );
        
    $personsAssoc= array(
    array('Firstname'=>'Johanna','Lastname'=> 'Den Doper', 'Age'=> 117,'Gender'=> 'Vrouw'),
    array('Firstname'=>'Mohammed','Lastname'=> 'El Farisi', 'Age'=> 40,'Gender'=> 'Man')
    
        );


?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Werken met arrays in php</title>
</head>
<body>
    
    <h1>met index</h1>
    <table>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Geslacht</th>
        </tr>
        <?php
        foreach($persons as $person)
        {?>
        
        <tr>
            <td><?php echo $person[0];?></td>
            <td><?php echo $person[1];?></td>
            <td><?php echo $person[2];?></td>
            <td><?php echo $person[3];?></td>
        </tr>
            
            <?php
        }?>
        
    </table>
    
    <h1>met string index</h1>
    <table>
        <tr>
            <th>Voornaam</th>
            <th>Familienaam</th>
            <th>Leeftijd</th>
            <th>Geslacht</th>
        </tr>
        <?php
        foreach($personsAssoc as $person)
        {?>
        
        <tr>
            <td><?php echo $person['Firstname'];?></td>
            <td><?php echo $person['Lastname'];?></td>
            <td><?php echo $person['Age'];?></td>
            <td><?php echo $person['Gender'];?></td>
        </tr>
            
            <?php
        }?>
        
    </table>
    
    
</body>
</html>