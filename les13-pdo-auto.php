<?php
    include ( __DIR__ . '/vendor/autoload.php');
    
    $appState = new \ModernWays\Dialog\Model\NoticeBoard();
    $request = new \ModernWays\Mvc\Request('/Home/Index');
    $route = new \ModernWays\Mvc\Route($appState, $request->uc());
    // de namespace waarin de klassen staan van mijn app/project
    // in de psr4 autoload moet ik dan het pad opgeven waar de klassen
    // van die namespace staan
    //
    // de volgende methode maakt een instantie van de klasse Home en voert de methode
    // index van die klasse uit op voorwaarde dat er geen andere route
    // wordt meegegeven.
    $routeConfig = new \ModernWays\Mvc\RouteConfig('\Programmeren4\Les13', $route, $appState);
    $view = $routeConfig->invokeActionMethod();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MVC webapp</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
</head>
<body>
     <div class="container body-content">
         
    <?php $view();?>
    </div>
</body>
</html>