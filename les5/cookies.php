<?php

$list = array();

if(!isset($_COOKIE["list"]))
            {
                //gebruik serialize om een array op te slagen in een cookie
                 setcookie("list", serialize($list), time() + (86400 * 30), "/");  
            }
            //gebruik unserialize om een array cookie op te slagen in een php array
            else{$list = unserialize( $_COOKIE["list"]);}

    if($_SERVER["REQUEST_METHOD"] == "POST")
        {
            
            array_push($list, $_POST["select"]);
            setcookie("list", serialize($list), time() + (86400 * 30), "/");
        }

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <h1>GSM winkel</h1>
     <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        <select name = "select">
                <option value="samsung">Samsung</option>
                <option value="iphone">Iphone</option>
                <option value="nokia">Nokia</option>
                <option value="windows">Windows</option>
        </select>
        
        <button type="submit">Verzenden</button>
</form>

<table>
    <tr>
        <th>Artikel</th>
    </tr>
    <?php
    
    foreach($list as $element){
    ?>
    <tr>
        <td><?php echo $element ?></td>
    </tr>
    
    <?php }?>
</table>



</body>
</html>