<?php
    
    session_start();
    $list = array();
    if(!isset($_SESSION["list"]))
    {
        $_SESSION["list"] = serialize($list);
    }
    else{$list = unserialize($_SESSION["list"]);}
    
    if($_SERVER["REQUEST_METHOD"] == "POST")
        {
            array_push($list, $_POST[select]);
            $_SESSION["list"] = serialize($list);
        }
        
    //hieronder wordt de if geopend wanneer we via een link een get uitvoeren    
    if(isset($_GET['remove'])) 
{
    //hieronder wordt de array van de sessie in $list gestoken
    if(isset($_SESSION['list'])) {
        $list = unserialize($_SESSION['list']);
    }
    //hieronder wordt het juiste element verwijderd uit de array en $_GET['remove'] is de index die we mee hebben gegeven
    //unset verwijderd het element maar de plaats blijft wel open
    unset($list[$_GET['remove']]);
    //de indexen worden opgeschoven
    $list = array_values($list);
    $_SESSION['list'] = serialize($list);
    //pagina vernieuwen
    header("Location: session.php");
}

if(isset($_GET['removeAll']))
{
    //sessie resetten dus alles wordt eruit verwijderd
    session_unset();
    //pagina resetten
    header("Location: session.php");
}


?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>webwinkel met Sessions</title>
    
</head>
<body>
    <h1>GSM winkel</h1>
     <form method="post" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']); ?>">
        <select name = "select">
                <option value="Samsung">Samsung</option>
                <option value="Iphone">Iphone</option>
                <option value="Nokia">Nokia</option>
                <option value="Windows">Windows</option>
        </select>
        
        <button type="submit">Verzenden</button>
</form>

<table>
    <tr>
        <th>Artikel</th>
    </tr>
    <?php
    //een teller om de positie waarop het element zich bevind in de sessie mee te geven aan de url ?remove
    $teller=0;
    foreach($list as $element){
    ?>
    <tr>
        <td><?php echo $element;?></td>
        <td> <?php
        //een link waarin we een GET request sturen en dat word bovenaan opgevangen.
        //ook wordt de de positie van het element dat verwijderd moet worden meegegeven 
        echo '<a href="session.php?remove='.$teller.'">Verwijder</a><br />'; 
        ?></td>
    </tr>
    
    <?php
    
    $teller++;
    }?>
</table>
<p>
<?php
//hieronder wordt er een link weergegeven om alle elementen te verwijderen uit de session
//als er geen elementen zijn wordt er "winkelwagen leeg" weergegeven.
if(count($list) != 0)
{
    echo '<a href="session.php?removeAll="true">Remove all</a><br />';
}
else{echo 'Winkelwagen leeg';}
 
?>
</p>


</body>
</html>