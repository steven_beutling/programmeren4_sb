<?php
$file = file_get_contents("Postcodes.csv");
$file = utf8_encode($file);

$lines = explode(PHP_EOL, $file);
$list = array();

foreach($lines as $line)
    {
        $temp = explode("|", $line);
        
        array_push($list,$temp);
    }

$color= array("black", "yellow", "red");
$teller = 0;

?>

<style type="text/css">
    table{
        
        border-collapse: collapse;
    }
    
    .black{
        background-color:black;
        color: white;
    }
    .yellow{
        background-color: yellow;
        color: black;
    }
    .red{
        background-color: red;
        color: white;
    }
    
    table, th, td {
    border: 1px solid black;
}

</style>

<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Postcodes</title>
</head>
<body>
    <table>
        <tr>
            <th>Postcode</th>
            <th>Gemeente</th>
            <th>Provincie</th>
            <th>Gemeente fr</th>
            <th>Provincie fr</th>
        </tr>
        <?php
        foreach($list as $item)
        {?>
        
        <tr class="<?php echo $color[$teller]?>">
            <td><?php echo $item[0];?></td>
            <td><?php echo $item[1];?></td>
            <td><?php echo $item[2];?></td>
            <td><?php echo $item[3];?></td>
            <td><?php echo $item[4];?></td>
        </tr>
            
            <?php
            if($teller == 2){$teller = 0;}else{$teller++;}
        }?>
</body>
</html>