<?php
//als het alleen php is op de pagina dan mag je hem niet afsluiten

function checkNum($number){
    
    if(!filter_var($number, FILTER_VALIDATE_INT)){
        throw new \Exception('De waarde is geen geheel getal');
    }
}
try{
    checkNum('66d');
    echo 'gelukt';
    
}
catch(\Exception $m){
    echo $m->getMessage();
}


