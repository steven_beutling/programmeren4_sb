<?php


class Notice
{
    public $title, $sender, $message, $publishedOn, $modifiedOn;
    
   
    
}

class Noticeboard
{
    private $notices = array();
    
    public function getNotices(){
        return $this->notices;
    }
    
    public function addNotice($noticeToAdd){
        
        
        $this->notices[] = $noticeToAdd;
        
    }
    
    
    
    public function removeNotice($index){
         unset($this->notices[$index]);
    
    $this->notices = array_values($this->notices);
   
    }
    
    
}



if(!isset($noticeboard)){
$noticeboard = new Noticeboard();
$notice1 = new Notice();
$notice1->title = 'titel1';
$notice1->sender = 'sender1';
$notice1->message = 'message1';
$notice1->publishedOn ='pub1';
$notice1->modifiedOn = 'mod1';
$noticeboard->addNotice($notice1);
}



if(isset($_GET['add'])){
$notice2 = new Notice();
$notice2->title = 'titel2';
$notice2->sender = 'sender2';
$notice2->message = 'message2';
$notice2->publishedOn ='pub2';
$notice2->modifiedOn = 'mod2';

$noticeboard->addNotice($notice2);
header("Location: classes.php");
}

if(isset($_GET['remove'])){
  
    $noticeboard->removeNotice($_GET['remove']);
    
    header("Location: classes.php");


}
 

?>

<!doctype html>
<html lang="nl">
<head>
    <meta charset="UTF-8">
    <title>Classes</title>
</head>
<body>
    <?php echo '<a href="classes.php?new=0">New</a><br />'; ?>
    <?php echo '<a href="classes.php?add=0">Add</a><br />'; ?>
    <table>
        <tr>
            <th>title</th>
            <th>sender</th>
            <th>message</th>
            
        </tr>
    <?php
    $teller=0;
    foreach($noticeboard->getNotices() as $item){
        ?>
        <tr>
            <td><?php echo $item->title ?></td>
            <td><?php echo $item->sender ?></td>
            <td><?php echo $item->message ?></td>
            <td><?php echo '<a href="classes.php?remove='.$teller.'">Verwijder</a><br />'; ?></td>
        </tr>
        <?php
        $teller++;
    }
    
    
    
    ?>
    
    </table>
</body>
</html>