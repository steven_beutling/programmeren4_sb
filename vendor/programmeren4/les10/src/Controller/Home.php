<?php
namespace Programmeren4\Les10\Controller;
class Home extends \ModernWays\Mvc\Controller
{
    public function index()
    {
        
        return $this->view('Home','Index');
    }
    public function postcodes(){
        
        $model = new \Programmeren4\Les10\Model\Postcode();
        $model->setList(\Programmeren4\Les10\Dll\DaFunctions::GetPostcodes());
        return $this->view('Home','postcodes',$model);
    }
}