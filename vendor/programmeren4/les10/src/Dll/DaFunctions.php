<?php
namespace Programmeren4\Les10\Dll;
class DaFunctions{
   public static function GetPostcodes(){
     $file = file_get_contents("Data/PostcodesLijst.csv");
        $file = utf8_encode($file);

        $lines = explode(PHP_EOL, $file);
        
        
        $lijst = array();
        
        foreach($lines as $line)
        {
            $gemeente = new \Programmeren4\Les10\Dll\Gemeente();
            $temp = explode("|", $line);
            
            $gemeente->postcode = $temp[0];
            $gemeente->gemeente = $temp[1];
            $gemeente->provincie = $temp[2];
            $gemeente->gemeenteFr = $temp[3];
            $gemeente->provincieFr = $temp[4];
            
            
        
        array_push($lijst,$gemeente);
        }
        return $lijst;
}
    
}


