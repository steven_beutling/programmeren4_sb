<?php
namespace Programmeren4\Les13\Controller;

class Home extends \ModernWays\Mvc\Controller
{
    
    public function index()
    {
        $model = new \Programmeren4\Les13\Model\Auto();
        
        try
        {
            $pdo = new \PDO('mysql:host=localhost;dbname=stevenbeutling;charset=utf8','stevenbeutling','');
            $command = $pdo->query("call ArticleSelectAll");
            $artikels = $command->fetchAll(\PDO::FETCH_ASSOC);
            $model->setList($artikels);
            
        }
        catch(\PDOException $e)
        {
            
        }
        
    
       
        
        return $this->view('Home','Index', $model);
    }
    public function inserting(){
        return $this->view('Home','Inserting', null);
    }
    
    
    public function insert()
    {
        
        
        try{
            $pdo3 = new \PDO('mysql:host=localhost;dbname=stevenbeutling;charset=utf8','stevenbeutling','');
            $command3 = $pdo3->query("call ArticleInsert('".$_POST['Naam']."','".$_POST['Datum']."','".$_POST['Prijs']."',@Id);");
            $v = $command3->fetchAll(\PDO::FETCH_ASSOC);
            
            }

            catch(\PDOException $e)
            {
                echo $e->getMessage();
            }
            return $this->index();
    }
    
    public function deleteArticle()
    {
        try{
                $pdo4 = new \PDO('mysql:host=localhost;dbname=stevenbeutling;charset=utf8','stevenbeutling','');
                $command4 = $pdo4->query("call ArticleDelete('".$_GET['remove']."')");
                $v = $command4->fetchAll(\PDO::FETCH_ASSOC);
            
        }
        catch(\PDOException $e)
            {
                echo $e->getMessage();
            }
        
        return $this->index();
    }
    public function updating(){
        $id = $_GET['update'];
        $model = new \Programmeren4\Les13\Model\Auto();
        
        try{
            $pdo2 = new \PDO('mysql:host=localhost;dbname=stevenbeutling;charset=utf8','stevenbeutling','');
        $command2 = $pdo2->query("call GetArticleById(".$id.")");
        $artikel = $command2->fetch(\PDO::FETCH_ASSOC);
        
        
        $model->id = $id;
        $model->name = $artikel['Name'];
        $model->price = $artikel['Price'];
        $model->PurchaseDate = $artikel['PurchaseDate'];
        }
         catch(\PDOException $e)
            {
                echo $e->getMessage();
            }
        
        return $this->view('Home', 'Update', $model);
        
    }
    public function update(){
        
        try{
            
        
        $name = $_POST['Naam'];
        $date = $_POST['Datum'];
        $price = $_POST['Prijs'];
        $id = $_GET['id'];
        $pdo2 = new \PDO('mysql:host=localhost;dbname=stevenbeutling;charset=utf8','stevenbeutling','');
        
        $command2 = $pdo2->query("call ArticleUpdate('".$name."', '".$date."', ".$price.", ".$id.")");
        $b = $command2->fetchAll(\PDO::FETCH_ASSOC);
        }
         catch(\PDOException $e)
            {
                echo $e->getMessage();
            }
        return $this->index();
    }
    
   
}