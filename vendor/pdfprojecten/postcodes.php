<?php
require('../setasign/fpdf/fpdf.php');

class PDF extends FPDF
{
    function loadData()
    {
        //Postcodes inladen
        $file = file_get_contents("Postcodes.csv");
        $file = utf8_encode($file);

        $lines = explode(PHP_EOL, $file);
        $list = array();

        foreach($lines as $line)
        {
            $temp = explode("|", $line);
        
            array_push($list,$temp);
        }
   
        return $list;
    }
// Page header
    function Header()
    {
	
    }

// Page footer
    function Footer()
    {
	// Position at 1.5 cm from bottom

    }
    
    function BasicTable($header, $data)
{
    // Column widths
    $w = array(40, 35, 40, 45);
    // Header
    for($i=0;$i<count($header);$i++)
        $this->Cell($w[$i],7,$header[$i],1,0,'C');
    $this->Ln();
    // Data
    foreach($data as $row)
    {
        $this->Cell($w[0],6,$row[0],'LR');
        $this->Cell($w[1],6,$row[1],'LR');
        
        $this->Ln();
    }
    // Closing line
    $this->Cell(array_sum($w),0,'','T');
    }
}


// Instanciation of inherited class
$pdf = new PDF();
$list = $pdf->loadData();
$header = array('Postcode', 'Gemeente');
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Arial','',8);
$pdf->BasicTable($header,$list);
//foreach($list as $item)
    //$pdf->Cell(0,10,$item[0] . "  " . $item[1],0,1);
	//$pdf->Cell(0,10,$item[0] . "  " . $item[1]. "  " . $item[2]. "  " .$item[3]. "  " .$item[4],0,1);
$pdf->Output();